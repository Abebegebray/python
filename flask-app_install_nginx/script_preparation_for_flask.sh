#!/usr/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 26/02/2021
#version: 1.0.0
########################################################################
clear
printf "%s\nCreating a Python Virtual env.\nInstlling all necessary tools.\nCreate directory and Enter to directory.\nCreate directory for virtual env.\nAnd create virtual env using Python3.\n\n"

opso=($(cat /etc/*-release | grep -E "^ID=" | cut -d"=" -f2 | tr -d \"))
printf "%s\nThe distribution name is #@# $opso #@#.\n\nChoice which of distribution famly is.\n"

while true; do
 read -p "Enter 'd' for Debian family, 'r' for Redhat family: " ops1
    case $ops1 in
        [Dd]* ) sudo apt sudo apt update;sudo insall python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools python3-venv git;pip install wheel;break;;
        [Rr]* ) sudo yum install epel-release platform-python-pip.noarch platform-python-devel.x86_64 gcc nginx git;python3 -m pip install --user --upgrade pip;python3 -m pip install --user virtualenv;break;;
        [Cc]* ) break;;
        * ) echo "Please enter d or r to continue c.";;
    esac
done

clear
read -p "What do you want to call your project:" myproj
cd ~/
rm -rf $myproj ~/$myproj > /dev/null
mkdir $myproj
cd $myproj/
mkdir ~/$myproj/$myproj
python3 -m venv ~/$myproj/$myproj

printf "%s\nActivate virtual anv.\n"
read -p "Enter to continue..."
source ~/$myproj/$myproj/bin/activate

printf "%s\nInstall Flask and gunicorn.\nAnd save requirement.\n"
sleep 2
pip install Flask gunicorn
pip freeze > requirements.txt

printf "%s\nClone the file of your project from gitlab.\n"
read -p "Enter username of gitlab: " username
read -p "Enter the name of directory you put your file app: " folderfile
git init
git pull https://gitlab.com/$username/$folderfile
read -p "Enter The name of file app to check it in localhost browser: " $filename
python ~/$myproj/$filename.py
