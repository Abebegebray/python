#!/usr/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 26/02/2021
#version: 1.0.0
########################################################################

printf "%s\nInstall nginx.\n\n"
t = "                                     #############"
opso=($(cat /etc/*-release | grep -E "^ID=" | cut -d"=" -f2 | tr -d \"))
clear
       printf "%s\n$t\nThe distribution name is ## $opso.\n$t\nChoice which of distribution famly is.\n"

while true; do
 read -p "Enter 'd' for Debian family, 'r' for Redhat family: " ops1
    case $ops1 in
        [Dn]* ) sudo apt install epel-release;sudo apt-get update;sudo apt install nginx;break;;
        [Rr]* )  sudo yum install epel-release;sudo yum update;sudo yum install nginx;break;;
        [Cc]* ) break;;
        * ) echo "Please enter d or r to continue c.";;
    esac
done
printf "%s\n\n Verify if installed nginx: $(sudo nginx -v)\n"
read -p "Enter to continue"
